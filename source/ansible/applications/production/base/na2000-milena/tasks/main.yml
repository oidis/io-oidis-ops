# * ********************************************************************************************************* *
# *
# * Copyright 2024 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

- name: "Register Docker user info"
  become: true
  ansible.builtin.user:
    name: "{{ oidis_user }}"
  check_mode: true
  register: docker_user_info

- name: "update application"
  environment:
    XDG_RUNTIME_DIR: "/run/user/{{ docker_user_info.uid }}"
    PATH: "/home/{{ oidis_user }}/.local/bin/:{{ docker_user_info.home }}/bin:{{ ansible_env.PATH }}"
    DOCKER_HOST: "unix:///run/user/{{ docker_user_info.uid }}/docker.sock"
  become: true
  become_user: "{{ oidis_user }}"
  block:
    - name: "ensure application directory"
      file:
        state: directory
        path: "{{ app_dir }}"
        owner: "{{ oidis_user }}"
        group: "{{ oidis_group }}"
#    - name: "turn on maintenance"
#      command:
#        cmd: "maintenance on"
#        chdir: "{{ app_dir }}"
#    - name: "delay"
#      pause:
#        seconds: 10
    - name: "check compose exists"
      stat:
        path: "{{ app_dir }}/compose.sh"
      register: compFileStats
    - name: "compose app down"
      command:
        cmd: "./compose.sh down || return 0"
        chdir: "{{ app_dir }}"
      when:
        - compFileStats.stat.exists
        - compFileStats.stat.executable
    - name: "docker prune"
      command:
        cmd: "docker system prune --force --all"
    - name: "copy new assets"
      template:
        src: "{{ item }}"
        dest: "{{ app_dir }}/{{ item | basename | regex_replace('\\.j2$', '') }}"
        mode: u=rw,g=r,o=r
      with_fileglob:
        - "{{ role_path }}/templates/*.j2"
    - name: "remove proxy configs"
      block:
        - name: "find files"
          find:
            paths: "{{ app_dir }}"
            patterns: "*.bridge.conf"
          register: proxyFiles
        - name: "delete proxy files"
          file:
            state: absent
            path: "{{ item.path }}"
          with_items: "{{ proxyFiles.files }}"
    - name: "copy proxy bridge assets"
      template:
        src: "{{ item }}"
        dest: "{{ app_dir }}/{{ item | basename | regex_replace('\\.j2$', '') }}"
        mode: u=rw,g=r,o=r
      with_fileglob:
        - "{{ role_path }}/templates/proxy/*.bridge.conf.j2"
    - name: "allow app script execution"
      file:
        path: "{{ app_dir }}/compose.sh"
        state: touch
        mode: u+x
    - name: "compose app pull"
      command:
        cmd: "./compose.sh pull"
        chdir: "{{ app_dir }}"
    - name: "compose app up"
      command:
        cmd: "./compose.sh up"
        chdir: "{{ app_dir }}"
#    - name: "delay"
#      pause:
#        seconds: 10
#    - name: "turn off maintenance"
#      command:
#        cmd: "maintenance off"
#        chdir: "{{ app_dir }}"
    - name: "proxy reload"
      command:
        cmd: "proxy-reload"
