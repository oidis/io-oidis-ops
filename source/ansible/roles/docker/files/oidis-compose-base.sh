#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

for i in "$@"; do
  case $i in
  -h | --help)
    echo "This is simple wrapper for docker compose to simplify invocation from this base overrides."
    echo "  start            Starts service"
    echo "  stop             Stops service and remove orphans"
    echo "  pull             Pull images"
    echo "  restart          Do the shallow restart"
    echo "  stats            Print docker stats"
    echo "  -f=*, --file=*   Specify docker compose file. Note that this could be already specified in wrapping script."
    exit 0
    ;;
  start | up)
    ACTION="up -d"
    shift
    ;;
  stop | down)
    ACTION="down --remove-orphans"
    shift
    ;;
  pull)
    ACTION="pull"
    shift
    ;;
  restart)
    ACTION="restart"
    shift
    ;;
  stats)
    ACTION="stats --no-stream"
    JUST_DOCKER=1
    shift
    ;;
  -f=* | --file=*)
    FILE="${i#*=}"
    shift
    ;;
  -*)
    echo "Unknown option $i" >>/dev/stderr
    exit 1
    ;;
  *) ;;
  esac
done

if [[ $JUST_DOCKER == 1 ]]; then
  docker $ACTION
else
  if [[ -z "${ACTION}" ]]; then
    echo "No task specified, see --help" >>/dev/stderr
    exit 1
  fi

  if [[ -z "${FILE}" ]]; then
    echo "Missing compose file argument, see --help" >>/dev/stderr
    exit 1
  fi
  docker-compose -f "$FILE" --compatibility $ACTION
fi

