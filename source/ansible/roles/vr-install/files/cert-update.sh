#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022-2023 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

# cron timing: recommended by letsecrypt and all registered domains should be processed by single plan
# do not forget to register cert-update.sh to /usr/bin
# sudo ln -sf $(pwd)/cert-update.sh /usr/bin cert-update
#  0 */12 * * * cert-update.sh

logfile=/var/log/oidis/cert-update.log
IS_FORCE=0
BACKUP_MODE=1
NO_BACKUP=0
DRY_RUN=0

log() {
  echo "$1"
  if [[ $DRY_RUN == 0 ]]; then
    echo "$1" >>$logfile
  fi
}

logError() {
  echo "$1" >>/dev/stderr
  if [[ $DRY_RUN == 0 ]]; then
    echo "ERROR: $1" >>$logfile
  fi
}

for i in "$@"; do
  case $i in
  -h | --help)
    echo "This is simple wrapper for certbot."
    echo "  -d=*, --domain=*    Domain for which certificate should be initialized or renewed"
    echo "  -e=*, --email=*     Email used for letsencrypt notifications"
    echo "  --init              Init certificate for selected specified domain (--domain=* is mandatory), renew certificate otherwise."
    echo "                      Renewing will be requested without this flag, and all maintained certificates will be renewed when domain "
    echo "                      is not specified"
    echo "  --force             Force run requested backup operation and ignore machine state."
    echo "  --list              Lists all maintained domains by certbot."
    echo "  --with-www          Generate domain with www alternative."
    echo "  --no-backup         Suppress backup of newly generated/created certs."
    echo "  --backup=*          Backup current letsencrypt as certs.tar.gz archive into specified folder."
    echo "  --restore=*         Restore letsencrypt from backup archive."
    echo "  --dry-run           Dry run, only print logs and suppress real execution."
    exit 0
    ;;
  -d=* | --domain=*)
    DOMAIN="${i#*=}"
    shift
    ;;
  -e=* | --email=*)
    EMAIL="${i#*=}"
    shift
    ;;
  --no-backup)
    NO_BACKUP=1
    shift
    ;;
  --restore=*)
    BACKUP_MODE=2
    BACKUP_PATH="${i#*=}"
    shift
    ;;
  --backup=*)
    BACKUP_MODE=1
    BACKUP_PATH="${i#*=}"
    shift
    ;;
  --init)
    IS_INIT=1
    shift
    ;;
  --force)
    IS_FORCE=1
    shift
    ;;
  --list)
    sudo certbot certificates | grep Certificate\ Name | awk '{ print $3 }'
    exit 0
    ;;
  --with-www)
    WITH_WWW=1
    shift
    ;;
  --dry-run)
    DRY_RUN=1
    shift
    ;;
  -*)
    logError "Unknown option $i"
    exit 1
    ;;
  *) ;;
  esac
done

if [[ $DRY_RUN == 0 ]]; then
  echo "" >>$logfile
  date >>$logfile
fi

function updateProxy() {
  log "Updating proxy"
  if [[ $DRY_RUN == 1 ]]; then
    log "dry-run: sudo nginx -s reload"
  else
    sudo nginx -s reload
  fi
}

function processCertbot() {
  if [[ $isMaster == 0 ]]; then
    log "This machine is not MASTER router, certbot task skipping"
    return 0
  fi

  if [[ $DRY_RUN == 1 ]]; then
    log "dry-run: certboting"
  else
    if [[ -z "${EMAIL}" ]]; then
      EMAIL="info@oidis.org"
    fi

    if [[ $IS_INIT == 1 ]]; then
      if [[ ! -f "/var/www/certbot" ]]; then
        sudo mkdir -p /var/www/certbot
      fi

      if [[ -z "${DOMAIN}" ]]; then
        logError "Domain needs to be specified for initialization, please use '--domain=<domain>' attribute."
        exit 1
      else
        log "Certificate initialization for: $DOMAIN"
        args=()
        if [[ $WITH_WWW == 1 ]]; then
          args=(-d "www.$DOMAIN")
        fi
        sudo certbot certonly -v -n --agree-tos -m "$EMAIL" --webroot --webroot-path /var/www/certbot -d "$DOMAIN" "${args[@]}" || exit 1
      fi
    else
      if [[ -z "${DOMAIN}" ]]; then
        log "Renewing all certificates..."
        sudo certbot renew --quiet
      else
        log "Renewing certificate for: $DOMAIN"
        sudo certbot renew --cert-name "$DOMAIN"
      fi
    fi
    updateProxy
  fi
}

function processRestore() {
  log "Starting certs restore from ${BACKUP_PATH}"
  if [[ $DRY_RUN == 1 ]]; then
    log "dry-run: certs restoring"
  else
    cd /tmp || exit 1
    tar -xf "${BACKUP_PATH}"

    if [[ -d "/tmp/certs/live" ]]; then
      if [[ ! -d "/etc/letsencrypt" ]]; then
        sudo mkdir -p "/etc/letsencrypt"
      else
        sudo rm -rf /etc/letsencrypt/*
      fi
      cp -r /tmp/certs/* /etc/letsencrypt
    fi
    rm -rf /tmp/certs
    updateProxy
  fi
}

function processBackup() {
  log "Starting certs backup to ${BACKUP_PATH}"
  if [[ $DRY_RUN == 1 ]]; then
    log "dry-run: certs backup"
  else
    backup_name="certs"

    mkdir -p /tmp/$backup_name
    cp -r /etc/letsencrypt/. /tmp/$backup_name
    cd /tmp || exit 1
    umask 177
    tar -czf /tmp/$backup_name.tar.gz $backup_name

    mv /tmp/$backup_name.tar.gz "${BACKUP_PATH}"
    rm -r /tmp/$backup_name
  fi
}

isMaster=0
if grep -Fxq MASTER /var/keepalived/actual.status; then
  isMaster=1
fi

if [[ $BACKUP_MODE == 2 ]]; then
  log "Restore requested, skipping certbot routine"
else
  processCertbot
fi

if [[ -n "${BACKUP_PATH}" && $NO_BACKUP == 0 ]]; then
  if [[ $BACKUP_MODE == 2 ]]; then
    if [[ $isMaster == 0 || $IS_FORCE == 1 ]]; then
      processRestore
    else
      log "Restore skipped, call this on not-master or use --force flag"
    fi
  elif [[ $BACKUP_MODE == 1 ]]; then
    if [[ $isMaster == 1 || $IS_FORCE == 1 ]]; then
      processBackup
    else
      log "Backup skipped, call this on master or use --force flag"
    fi
  else
    logError "Undefined backup operation mode, use backup or restore argument"
  fi
fi
