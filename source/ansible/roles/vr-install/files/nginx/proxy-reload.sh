#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

echo -e "\033[0;32m > reloading proxy..\033[0m"
sudo nginx -t || exit 1
sudo nginx -s reload
echo -e "\033[1;32m > proxy reload executed\033[0m"
