KEY_SHA512="e09fa32f0a0eab2b879ccbbc4d0e4fb9751486eedda75e35fac65802cc9faa266425edf83e261137a2f4d16281ce2c1a5f4502930fe75154723da014214f0655"
wget -O /tmp/nginx_signing.rsa.pub https://nginx.org/keys/nginx_signing.rsa.pub
if echo "$KEY_SHA512 */tmp/nginx_signing.rsa.pub" | sha512sum -c -; then
  echo "key verification succeeded!"
  mv /tmp/nginx_signing.rsa.pub /etc/apk/keys/
else
  echo "key verification failed!"
  exit 1
fi
