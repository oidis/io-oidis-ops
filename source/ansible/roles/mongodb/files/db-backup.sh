#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022-2025 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *
application="backup"
deploy="nexus"
dbHost="127.0.0.1"
dbAuth="admin"
dumpProxy=""
dryRun=0
tempPath="/tmp"
logFile=$tempPath/db-backup.log
inputPath=""
archiveType="tar.gz"

function log() {
  echo "$@"
  # shellcheck disable=SC2145
  echo "[$(date -u)][INFO]: $@" >>$logFile
}

function logError() {
  echo "$@" >>/dev/stderr
  # shellcheck disable=SC2145
  echo "[$(date -u)][ERROR]: $@" >>$logFile
}

function help() {
  echo "This is generic DB backup tool which is able to dump MongoDB and MySQL databases and deploy the dump to nexus or mounted disk."
  echo "  --help            Prints help."
  echo "  --dry-run         Use this flag to disable real fs/terminal manipulation and print only resolved commands instead."
  echo "  --profile=*       Specify one of dev|eap|prod profile."
  echo "  --type=*          Select one of supported database mongo|mysql|dir."
  echo "  --app=*           Specify application name or namespace."
  echo "  --db-name=*       Database table/document name to backup."
  echo "  --db-host=*       Database host."
  echo "  --db-port=*       Database port."
  echo "  --db-user=*       Database user with permissions to specified DB table/document."
  echo "  --db-pass=*       Database user password with permissions to specified DB table/document."
  echo "  --db-auth=*       Select authentication database (mongo only)."
  echo "  --deploy=*        Select one of supported deploy type nexus|dir|ftp."
  echo "  --deploy-url=*    Specify deploy server URL. URL will be concatenated with path option. (i.e. ftp://'<user>':'<pass>'@<url>)"
  echo "  --path=*          Specify path to deploy (relevant for dir and FTP deploy type right now)."
  echo "  --dumpProxy=*     This option will switch on 'docker exec <specified container> mongodump' proxy instead of local execution."
  echo "  --input-path=*    Specify directory to create backup from. Useful with --type=dir."
  echo "  --archive-type=*  Selects one of supported archive types [tar.gz, zip]. (tar.gz is default)"
  echo "  --encrypt-pass=*  Specify token or password for backup archive encryption."
}

function argParse() {
  for i in "$@"; do
    case $i in
    -h | --help)
      help
      exit 0
      ;;
    --profile=*)
      profile="${i#*=}"
      shift
      ;;
    --type=*)
      type="${i#*=}"
      shift
      ;;
    --app=*)
      application="${i#*=}"
      shift
      ;;
    --db-name=*)
      dbName="${i#*=}"
      shift
      ;;
    --db-host=*)
      dbHost="${i#*=}"
      shift
      ;;
    --db-port=*)
      dbPort="${i#*=}"
      shift
      ;;
    --db-user=*)
      dbUser="${i#*=}"
      shift
      ;;
    --db-pass=*)
      dbPass="${i#*=}"
      shift
      ;;
    --db-auth=*)
      dbAuth="${i#*=}"
      shift
      ;;
    --deploy=*)
      deploy="${i#*=}"
      shift
      ;;
    --deploy-url=*)
      deployURL="${i#*=}"
      shift
      ;;
    --path=*)
      deployPath="${i#*=}"
      shift
      ;;
    --input-path=*)
      inputPath="${i#*=}"
      shift
      ;;
    --dumpProxy=*)
      dumpProxy="${i#*=}"
      shift
      ;;
    --dry-run)
      dryRun=1
      shift
      ;;
    --archive-type=*)
      archiveType="${i#*=}"
      shift
      ;;
    --encrypt-pass=*)
      encryptPass="${i#*=}"
      shift
      ;;
    -*)
      logError "Unknown option $i"
      help
      exit 1
      ;;
    *) ;;
    esac
  done
}

function validateArgs() {
  if [[ $type != "mongo" && $type != "mysql" && $type != "dir" ]]; then
    logError "DB type needs to be specified, please add --type=[mongo|mysql|dir] and run again"
    exit 1
  fi

  if [[ $deploy != "nexus" && $deploy != "dir" && $deploy != "ftp" ]]; then
    logError "DB type needs to be specified, please add --type=[nexus|dir|ftp] and run again"
    exit 1
  fi

  if [[ $deploy == "dir" || $deploy == "ftp" ]]; then
    if [[ -z $deployPath ]]; then
      logError "Path needs to be specified for selected deploy type: $deploy. Please add --path=<path> option."
      exit 1
    elif [[ $deployPath != */ ]]; then
      deployPath="$deployPath/"
    fi
  fi

  if [[ $type == "dir" ]]; then
    if [[ -z $inputPath ]]; then
      logError "Input path needs to be specified for dir backup type. Please add --input-path=<dir-path> option."
      exit 1
    fi
  elif [[ -z ${dbName} ]]; then
    logError "Database name (--db-name=<name>) needs to be specified"
    exit 1
  fi

  if [[ $archiveType != "tar.gz" && $archiveType != "zip" ]]; then
    logError "Unsupported archiveType value '$archiveType' please use --help to list supported values"
    exit 1
  fi

  if [[ -n "${encryptPass}" && ${#encryptPass} -lt 8 ]]; then
    logError "Encryption password is too short (minimum 8 characters required), or remove --encrypt-pass argument from CLI to disable encryption."
    exit 1
  fi
}

function runCommand() {
  if [[ $dryRun == 1 ]]; then
    # shellcheck disable=SC2145
    log ">> dry-run: ${@}"
    return 0
  fi
  # shellcheck disable=SC2294
  if (eval "$@"); then
    return 0
  else
    logError "Command execution failed"
    exit 1
  fi
}

function backupMongo() {
  dumpName="${application}Dump_$(date '+%Y%m%d%H%M%S').$archiveType"
  dumpPath="/tmp/$dumpName"

  if [[ -z ${dbPort} ]]; then
    dbPort=27017
  fi
  args=()
  if [[ -n "$dbUser" ]]; then
    if [[ -z "$dbPass" ]]; then
      logError "Missing password, please add --db-pass=<password>"
      exit 1
    fi
    args=(--username="$dbUser" --password="$dbPass" --authenticationDatabase="$dbAuth")
  fi
  if [[ -n "$dumpProxy" ]]; then
    log "Starting dump from container..."
    runCommand docker exec "$dumpProxy" mongodump --host="$dbHost" --port="$dbPort" --db="$dbName" --out="/tmp/${application}Dump" --forceTableScan "${args[@]}"
    runCommand docker cp "$dumpProxy:/tmp/${application}Dump" "${application}Dump"
    runCommand docker exec "$dumpProxy" rm -rf "/tmp/${application}Dump"
  else
    log "Starting dump from connection..."
    runCommand mongodump --host="$dbHost" --port="$dbPort" --db="$dbName" --out="${application}Dump" --forceTableScan "${args[@]}"
  fi
  log "Create archive from dumped data - ${application}"
  if [[ $archiveType == "tar.gz" ]]; then
    runCommand tar -czvf "$dumpPath" "${application}Dump"
  else
    runCommand zip -q -r "$dumpPath" "${application}Dump"
  fi
  runCommand rm -rf "${application}Dump"
}

function backupMysql() {
  dumpName="${application}Dump_$(date '+%Y%m%d%H%M%S').sql"
  dumpPath="/tmp/$dumpName"

  if [[ -z ${dbPort} ]]; then
    dbPort=3306
  fi
  args=()
  if [[ -n "$dbUser" ]]; then
    if [[ -z "$dbPass" ]]; then
      logError "Missing password, please add --db-pass=<password>"
      exit 1
    fi
    args=(--user="$dbUser" --password="$dbPass")
  fi
  if [[ -n "$dumpProxy" ]]; then
    log "Starting dump from container..."
    runCommand docker exec "$dumpProxy" mysqldump -alv --single-transaction --host="$dbHost" --port=$dbPort "${args[@]}" --result-file="/tmp/dump.tar.gz" "$dbName"
    runCommand docker cp "$dumpProxy:/tmp/dump.tar.gz" "$dumpPath"
    runCommand docker exec "$dumpProxy" rm -rf "/tmp/dump.tar.gz"
  else
    log "Starting dump from connection..."
    runCommand mysqldump -alv --single-transaction --host="$dbHost" --port=$dbPort "${args[@]}" --result-file="$dumpPath" "$dbName"
  fi
}

function backupDir() {
  dumpName="${application}Blob_$(date '+%Y%m%d%H%M%S').$archiveType"
  dumpPath="/tmp/$dumpName"
  ipRoot=$(dirname "$inputPath")
  ipName=$(basename "$inputPath")

  log "Starting dump from input directory"
  if [[ $archiveType == "tar.gz" ]]; then
    runCommand tar -czf "$dumpPath" -C "$ipRoot" "$ipName"
  else
    runCommand "cd \"$ipRoot\" && zip -q -r \"$dumpPath\" \"$ipName\""
  fi
}

function deployNexus() {
  if [[ $profile != "dev" && $profile != "eap" && $profile != "prod" ]]; then
    logError "Profile needs to be specified for nexus deploy, please add --profile=[dev|eap|prod] and run again"
    exit 1
  fi

  log "Dump upload to nexus"
  runCommand oidis nexus:"$profile" --no-target --file="$dumpPath"
}

function deployFolder() {
  output=$deployPath
  if [[ -d "$deployPath" ]]; then
    output="$deployPath/$dumpName"
  else
    output=$deployPath
  fi

  if [[ -f "$(dirname "$output")" ]]; then
    logError "Output directory not exists: $output"
    exit 1
  fi

  log "Copy archived dump to $output"
  if [[ $dryRun == 0 ]]; then
    runCommand cp "$dumpPath" "$output"
  fi
}

function deployFTP() {
  log "Dump upload to FTP server"
  runCommand curl -T "\"$dumpPath\" \"$deployURL$deployPath\""
}

function cleanUp() {
  log "Clean up"
  runCommand rm "$dumpPath"
}

function encrypt(){
  # decrypt: openssl enc -d -aes-256-cbc -pbkdf2 -in backup.enc -out backup.tar.gz -pass pass:"$encryptPass"
  log "Starting encryption of $dumpPath"
  runCommand openssl enc -aes-256-cbc -salt -pbkdf2 -in "$dumpPath" -out "$dumpPath.enc" -pass pass:"$encryptPass"
  log "Encryption succeed, removing raw file"
  runCommand rm "$dumpPath"
  dumpName="$dumpName.enc"
  dumpPath="$dumpPath.enc"
  log "Encrypted file $dumpPath is ready to deploy"
}

function main() {
  argParse "$@"
  validateArgs

  if [[ $type == "mongo" ]]; then
    backupMongo
  elif [[ $type == "mysql" ]]; then
    backupMysql
  else
    backupDir
  fi

  if [[ -n "${encryptPass}" ]]; then
    encrypt
  fi

  if [[ $deploy == "nexus" ]]; then
    deployNexus
  elif [[ $deploy == "ftp" ]]; then
    deployFTP
  else
    deployFolder
  fi

  cleanUp
}

main "$@"
