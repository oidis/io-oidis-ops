# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

server {
    server_name ops.oidis.io;
    listen 80;

    location / {
        return 301 https://ops.oidis.io$request_uri;
    }

    include /var/oidis/tools/certutil.conf;
}

server {
    server_name ops.oidis.io;
    listen 443 ssl;

    ssl_certificate /etc/letsencrypt/live/ops.oidis.io/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/ops.oidis.io/privkey.pem;

    location / {
        proxy_pass http://localhost:8080/;

        proxy_buffering off;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_headers_hash_max_size 512;
    }
}
