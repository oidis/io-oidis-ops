#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

./db-backup.sh --type=mysql --db-name=zabbix --db-user=zabbix --db-pass=ZiSTnJM8y1JKxg1 --app=zabbix --path=/mnt/wedos-intern/backup/ops-zabbix

# crontab -e
# 0 0 */7 * * /var/oidis/zbx-backup.sh

# datagrip
#  create SSH tunneled connection to container
#  after that create zabbix user with default location and password
#  crate database zabbix and add all permissions to zabbix user
#    dialog selection didnt work so open console in datagrip and run: GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'%' WITH GRANT OPTION;
#    in the case of some failures the root user may need (not verified): GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;

# datagrip has option to import data from sql but it needs table specification, and no idea how to handle this, so use SQL script instead
# open datagrip -> context menu -> SQL scripts -> Run SQL script | select dumped <file>.sql
