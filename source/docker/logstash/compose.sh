#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2023 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

oidis-compose-base.sh --file=logstash.docker.yml "${@}"
