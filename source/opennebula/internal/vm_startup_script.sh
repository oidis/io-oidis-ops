FILE=/root/.initialized
if test -f "$FILE"; then
  echo "VM already initialized"
else
  useradd -m -p "$(echo JDEkVDRPMXphWGEkL1ZrM1Y1YldmUi4vaEEvNmU4ZFJYLwo= | base64 --decode)" -s /usr/bin/bash oidis
  usermod -aG sudo oidis

  # add internal cloud key
  ckey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFJS3cZ47g+3K1TqWntZLB3YMNxjnd+H3VSevzmoaFom1EjP33i8CB3/oGYN02npd7jUisj9BvRNcDrE3ENuw8sWTQgANDarj3psZ5izs5bjB9dzYbePLyT2QU465JMmHgLUZ/VNLhjh6v8hUFFGP3BGxwtvgSThkkFIs6MSgfbNvoQrHm6L2Zu1FT4N27S8N3ksknThiPdRtfClkcNGQaWC78ICcc3dmBK0VHNEuLszT0Szrh/i3XJpFZ+736pBP8a4bsjSZyqjYuMfhyt6O5Pu9ylcyJcJfbQEE9IXQ5pdOO5XVAmVvoZL0GmoX3zxlngGdtnyNIfleX7ACl4+3cQ57ni7BX2nPcD2KeoKE3hFoJx4L2x0/KZ3C6AnbiRfcE9UC7k2rMSYX4bL4VXbuB3WHPJLeBPCBBtZ4Hle0EYIZMgWWucpm47fxvKl5efMKecXOINHUo3g4hjvjw4GMu6CYrBd3xfkcCVq3W1cdxyIUlVvg5qwQ3m0rFN3O0tMc= oidis@oidis.cloud"
  echo "$ckey" >>/root/.ssh/authorized_keys
  # copy original key from opennebula to new user
  mkdir /home/oidis/.ssh
  cp /root/.ssh/authorized_keys /home/oidis/.ssh/authorized_keys
  chown -R oidis:oidis /home/oidis/.ssh

  cfg="
Include /etc/ssh/sshd_config.d/*.conf
PermitRootLogin yes
#PubkeyAuthentication yes
PasswordAuthentication no
PermitEmptyPasswords no
KbdInteractiveAuthentication no
UsePAM yes
AcceptEnv LANG LC_*
Subsystem       sftp    /usr/lib/openssh/sftp-server
"
  echo "$cfg" >/etc/ssh/sshd_config

  systemctl restart sshd
  echo "initialized" >$FILE
fi
