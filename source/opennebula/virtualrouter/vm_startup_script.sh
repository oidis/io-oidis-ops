# add rules
iptables -t nat -A POSTROUTING --dst 10.0.0.0/8 -j ACCEPT
iptables -t nat -A POSTROUTING --dst 192.168.0.0/16 -j ACCEPT
iptables -t nat -A POSTROUTING --dst 172.16.0.0/12 -j ACCEPT
iptables -t nat -A POSTROUTING -j MASQUERADE
# remove duplicates
iptables-save | awk '!COMMIT||!x[$0]++' | iptables-restore

FILE=/root/.initialized
if test -f "$FILE"; then
  echo "VM already initialized"
else
  ## this is our injection
  # copy internal oidis_cloud key
  ckey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFJS3cZ47g+3K1TqWntZLB3YMNxjnd+H3VSevzmoaFom1EjP33i8CB3/oGYN02npd7jUisj9BvRNcDrE3ENuw8sWTQgANDarj3psZ5izs5bjB9dzYbePLyT2QU465JMmHgLUZ/VNLhjh6v8hUFFGP3BGxwtvgSThkkFIs6MSgfbNvoQrHm6L2Zu1FT4N27S8N3ksknThiPdRtfClkcNGQaWC78ICcc3dmBK0VHNEuLszT0Szrh/i3XJpFZ+736pBP8a4bsjSZyqjYuMfhyt6O5Pu9ylcyJcJfbQEE9IXQ5pdOO5XVAmVvoZL0GmoX3zxlngGdtnyNIfleX7ACl4+3cQ57ni7BX2nPcD2KeoKE3hFoJx4L2x0/KZ3C6AnbiRfcE9UC7k2rMSYX4bL4VXbuB3WHPJLeBPCBBtZ4Hle0EYIZMgWWucpm47fxvKl5efMKecXOINHUo3g4hjvjw4GMu6CYrBd3xfkcCVq3W1cdxyIUlVvg5qwQ3m0rFN3O0tMc= oidis@oidis.cloud"
  echo "$ckey" >>/root/.ssh/authorized_keys
  sort /root/.ssh/authorized_keys | uniq >/root/.ssh/authorized_keys.uniq
  mv -f /root/.ssh/authorized_keys{.uniq,}

  # upgrade system
  apk update
  apk upgrade
  apk add python3

  # disable ipv6 to prevent awall issues
  echo "net.ipv6.conf.all.disable_ipv6 = 1" >>/etc/sysctl.conf
  echo "net.ipv6.conf.default.disable_ipv6 = 1" >>/etc/sysctl.conf
  echo "net.ipv6.conf.lo.disable_ipv6 = 1" >>/etc/sysctl.conf
  sysctl -p

  cfg="
### Oidis - Virtual router
Port 33666
PermitRootLogin yes
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
PasswordAuthentication no
PermitEmptyPasswords no
HostbasedAuthentication no
ChallengeResponseAuthentication no
AllowAgentForwarding yes
AllowTcpForwarding yes
GatewayPorts no
X11Forwarding no
TCPKeepAlive yes
PermitTunnel yes
Subsystem       sftp    /usr/lib/ssh/sftp-server
"
  echo "$cfg" >/etc/ssh/sshd_config
  rc-service sshd restart
  echo "initialized" >$FILE
fi
