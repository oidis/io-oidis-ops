#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2025 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

imageName="$1"
timeout="10"
tempPath="/tmp"
logFile=$tempPath/docker-image-validation.log

function log() {
  echo "$@"
  # shellcheck disable=SC2145
  echo "[$(date -u)][INFO]: $@" >>$logFile
}

function logError() {
  echo "$@" >>/dev/stderr
  # shellcheck disable=SC2145
  echo "[$(date -u)][ERROR]: $@" >>$logFile
}

function help() {
  echo "This script purpose is to validate if docker image is properly constructed for an application."
  echo "use:"
  echo " docker-image-validate.sh [attributes] <image name>"
  echo "  --help         Prints help."
  echo "  --timeout=*    Specify timeout in seconds [default=10]."
}

function argParse() {
  for i in "$@"; do
    case $i in
    -h | --help)
      help
      exit 0
      ;;
    --timeout=*)
      timeout="${i#*=}"
      shift
      ;;
    *)
      imageName="$1"
      shift
      ;;
    esac
  done
}

function validateArgs() {
  if [[ -z "$imageName" ]]; then
      logError "Missing image name. See help:"
      help
      exit 1
  fi

  if [[ ! "${timeout}" =~ ^[0-9]+$ ]]; then
    logError "Explicitly specified timeout is NaN."
    exit 1
  fi
}

function main() {
    argParse "$@"
    validateArgs

    log "Container preparation..."
    container=$(docker run -d --rm --name "integrity_test_$(date +%s)" --restart no "$imageName" 2>/dev/null)

    if [ -z "$container" ]; then
        logError "Container preparation failed: $imageName"
        exit 2
    fi
    log "Container $container started, waiting for $timeout seconds"
    sleep "$timeout"

    log "Starting validation..."

    if docker ps -q --filter "id=$container" | grep -q .; then
      log "Image $imageName validation succeed, stopping container $container"
        docker stop "$container" >/dev/null
        exit 0
    else
        logError "Image $imageName validation failed, please validate image consistency."
        exit 3
    fi
}

main "$@"
