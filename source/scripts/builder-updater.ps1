# * ********************************************************************************************************* *
# *
# * Copyright 2022-2025 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *
$hubUrl = "hub.oidis.io"
$builderType = "shared"
$builderConfName = "OidisBuilder.config.jsonp"
$builderOs = "win"
$builderSrc = "io-oidis-builder"
$builderCmdScript = "oidis.sh"
$confPath = ""

if ($IsWindows -or $ENV:OS)
{
    $builderCmdScript = "oidis.cmd"
    $tempPath = Join-Path $env:TEMP "selfupdate-script"
    if (-not(Test-Path $tempPath))
    {
        new-item "$tempPath" -ItemType Directory -Force
    }
}
else
{
    $tempPath = "/tmp"
}

function printHelp()
{
    Write-Output "Builder updater."
    Write-Output "  --help              Prints help."
    Write-Output "  --builder-src=*     Builder source path (destination)."
    Write-Output "  --import-cfg=*      Import private config from path."
}

function argParse($argList)
{
    for ($i = 0; $i -lt $argList.count; $i++) {
        switch ($argList[$i])
        {
            "-h" {
            }
            "--help" {
                printHelp
                exit 0;
            }
            "--builder-src"{
                $script:builderSrc = $argList[++$i]
                break
            }
            "--import-cfg"{
                $script:confPath = $argList[++$i]
                break
            }
            default {
                Write-Error "Unknown option: $( $argList[$i] )"
                exit 1
            }
        }
    }
}

argParse $args

Write-Output "Running with:"
Write-Output "  builderOs:         $builderOs"
Write-Output "  builderSrc:        $builderSrc"
Write-Output "  builderCmdScript:  $builderCmdScript"
Write-Output "  hubUrl:            $hubUrl"
Write-Output "  builderType:       $builderType"
Write-Output "  confPath:          $confPath"
Write-Output " --- "

if (-Not($builderOs) -or -Not($builderSrc))
{
    Write-Error "Builder OS and source path needs to be specified"
    exit 1
}
if (-Not($hubUrl) -or -Not($builderType))
{
    Write-Error "Hub location and builder platform type needs to be defined"
    exit 1
}

$confData = ""
if (Test-Path "$builderSrc/$builderConfName")
{
    Write-Output "Reading private config data"
    $confData = Get-Content -Path "$builderSrc/$builderConfName"
    Copy-Item "$builderSrc/$builderConfName" -Destination "$tempPath/$builderConfName"
    Write-Output "Private config backup: $tempPath/$builderConfName"
}

if (-Not(Test-Path "$builderSrc"))
{
    new-item "$builderSrc" -ItemType Directory -Force
}

$builderSrc = Resolve-Path "$builderSrc"

Write-Output "Downloading builder..."
try
{
    Invoke-WebRequest -Uri "https://$hubUrl/Update/io-oidis-builder/null/$builderType-$builderOs-nodejs" -OutFile "$tempPath/builder.zip"
}
catch
{
    Write-Error "Failed to download new builder package: $( $_.Exception )"
    exit 1
}

Write-Output "Unpacking builder..."

Remove-Item -Path "$builderSrc/*" -Force -Recurse

Expand-Archive -Path "$tempPath/builder.zip" -DestinationPath "$builderSrc"
Remove-Item -Path "$tempPath/builder.zip" -Force

Copy-Item -Path "$builderSrc/io-oidis-builder-*/*" -Destination "$builderSrc" -Recurse -Container
Remove-Item -Path "$builderSrc/io-oidis-builder-*" -Force -Recurse

Write-Output "Builder prepared"

if ($confData)
{
    Write-Output "Loading private config from backup"
    $confData | Out-File -FilePath "$builderSrc/$builderConfName" -Encoding utf8
}
else
{
    if (($confPath) -and (Test-Path $confPath))
    {
        Copy-Item "$confPath" -Destination "$builderSrc/$builderConfName"
        Write-Output "Importing private config from external path"
    }
    else
    {
        Write-Output "Import of private config '$confPath' skipped: source file not found"
    }

}

function wuiPathRegistration()
{
    Write-Warning "Please add '$builderSrc/cmd' directory into environment search path (i.e.: environment.PATH)"
}

if (Get-Command "oidis" -ErrorAction SilentlyContinue)
{
    Write-Output "oidis command found in system"
    $wuiPath = (Get-Command "oidis").Path
    if (-Not($IsWindows -or $ENV:OS))
    {
        $wuiPath = & readlink "$wuiPath"
    }
    Write-Output "oidis-result: $wuiPath"

    if (-Not("$wuiPath" -eq $( Get-Command "$builderSrc/cmd/$builderCmdScript" -ErrorAction SilentlyContinue ).Path))
    {
        wuiPathRegistration
    }
}
else
{
    wuiPathRegistration
}

Write-Output "Builder updated"
